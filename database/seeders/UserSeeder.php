<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        DB::table('users')->insert([
            [
                'email' => 'usuario_01@hotmail.com',
                'name' => 'usuario_01',
                'password' => bcrypt('asd12345')
            ],
            [
                'email' => 'usuario_02@hotmail.com',
                'name' => 'usuario_02',
                'password' => bcrypt('fgh12345')
            ],
            [
                'email' => 'usuario_03@hotmail.com',
                'name' => 'usuario_03',
                'password' => bcrypt('jkl12345')
            ],
            [
                'email' => 'usuario_04@hotmail.com',
                'name' => 'usuario_04',
                'password' => bcrypt('qwe12345')
            ],
            [
                'email' => 'usuario_05@hotmail.com',
                'name' => 'usuario_05',
                'password' => bcrypt('rty12345')
            ]
            ]);
    }
}
