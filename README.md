# Auth Api

Crear un scheme llamado _auth_api_ en mysql

crear una copia del archivo _.env.example_ y renombrarlo a _.env_

conectar la base de datos desde el archivo _.env_

## Importar las librerias
_php install_

## Crear las tablas en el DB
_php artisan migrate_


## LLenar las tablas en el DB
_php artisan db:seed_


## Establecer el servidor local
_php artisan serve_


## Para validar (POST) usuario y contraseña:
_http://localhost:8000/api/login_

| Usuario | Contraseña |
| ------ | ------ |
| usuario_01 | asd12345 |
| usuario_02 | fgh12345 |
| usuario_03 | jkl12345 |
| usuario_04 | qwe12345 |
| usuario_05 | rty12345 |


## Para ver (GET) información del usuario:    
_http://localhost:8000/api/profile_
