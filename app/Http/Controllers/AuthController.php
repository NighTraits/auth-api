<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(Request $request){
            $fields = $request -> validate([
                'name' => 'required|string',
                'password' => 'required|string'
            ]);
    
            // check email
            $user = User::WHERE('name', $fields['name'])->first();
    
            // check password
            if(!$user || !Hash::check($fields['password'], $user->password)){
                return response ([
                    'message' => 'Usuario o contraseña invalida'
                ], 401);
            }
    
            $token = $user->createToken('myapptoken')->plainTextToken;
    
            $response = [
                'user' => $user,
                'token' => $token
            ];
    
            return response($response, 201);
            return 'Sesion iniciada.';
        }

        public function logout(Request $request){
            auth()->user()->tokens()->delete();
            return [
                'message' => 'Logged out'
            ];
        }

        public function profile(Request $request){
            $user = auth()->user();
            return User::find($user);
        }
    
}
